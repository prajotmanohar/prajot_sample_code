package com;

public class Habits {
	private String somok;
	private String alcohol;
	private String dExercise;
	private String drugs;
	
	public Habits(String somok, String alcohol,
			String dExercise,String drugs ) {
		this.somok=somok;
		this.alcohol=alcohol;
		this.dExercise=dExercise;
		this.drugs=drugs;
	}
	
	public String getSomok() {
		return somok;
	}
	public void setSomok(String somok) {
		this.somok = somok;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getdExercise() {
		return dExercise;
	}
	public void setdExercise(String dExercise) {
		this.dExercise = dExercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	
	

}
