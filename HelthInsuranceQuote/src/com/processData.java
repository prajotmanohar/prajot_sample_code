package com;

public class processData {
	
	DataManager dataManager = new DataManager();
	
	public int getInsurenceProcessed() {
		int age = Integer.parseInt(dataManager.getData().getAge());
		String gender = dataManager.getData().getGender();
		HealthCondition condition =  dataManager.getData().getHealth();
		Habits habit = dataManager.getData().getHabit();
		int premium = 5000;
		
		if (age == 18) {
			premium = 5000;
		} else if (age > 18 && age < 40) {
			premium = premium + getPercentageAmount(premium, 10);
		} else if (age >= 40) {
			premium = premium + getPercentageAmount(premium, 20);
		}
		
		if (gender == "male") {
			premium = premium + getPercentageAmount(premium, 20);
		}
		int cflag = 0;
		if (condition.getbPressure().equalsIgnoreCase("yes")) {
			cflag++;
		}
		if (condition.getbSugar().equalsIgnoreCase("yes")) {
			cflag++;
		}
		if (condition.getHypertension().equalsIgnoreCase("yes")) {
			cflag++;
		}
		if (condition.getOverweight().equalsIgnoreCase("yes")) {
			cflag++;
		}
		int hflag = 0;
		if (habit.getAlcohol().equalsIgnoreCase("yes")) {
			hflag = hflag + 3;
		}
		if (habit.getdExercise().equalsIgnoreCase("yes")) {
			hflag = hflag + 3;
		}
		if (habit.getDrugs().equalsIgnoreCase("yes")) {
			hflag = hflag + 3;
		}
		
		
		premium += getPercentageAmount(premium, cflag);
		if (hflag == 0) {
			premium -= getPercentageAmount(premium, hflag);
		} else {
			premium += getPercentageAmount(premium, hflag);
		}
		
		return premium;
	}
	public int getPercentageAmount(int amount, int per) {
		return (amount + per) / 100;
	}

}
