package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataManager {
	private ReadData data = null;
	
	public ReadData getData() {
		return data;
	}

	public void setData(ReadData data) {
		this.data = data;
	}

	public DataManager() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("insurance.config");
			prop.load(input);
			
			Habits habit = new  Habits(prop.getProperty("smok","no"),
					prop.getProperty("alcohol","no"),
					prop.getProperty("dExercise","no"),
					prop.getProperty("drugs","no"));
			
			HealthCondition condition = new HealthCondition(
					prop.getProperty("hypertension","no"),
					prop.getProperty("bPressure","no"),
					prop.getProperty("bSugar","no"),
					prop.getProperty("overweight","no"));
			ReadData readData = new ReadData(prop.getProperty("name","Norman Gomes"),
					prop.getProperty("gender","male"),
					prop.getProperty("age","34"),
					habit,condition);
			this.setData(readData);
						
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
