package com;

public class HealthCondition {
	private String hypertension;
	private String bPressure;
	private String bSugar;
	private String overweight;
	

	public HealthCondition(String hypertension, String bPressure,
			String bSugar, String overweight) {
		this.hypertension = hypertension;
		this.bPressure = bPressure;
		this.bSugar = bSugar;
		this.overweight = overweight;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getbPressure() {
		return bPressure;
	}
	public void setbPressure(String bPressure) {
		this.bPressure = bPressure;
	}
	public String getbSugar() {
		return bSugar;
	}
	public void setbSugar(String bSugar) {
		this.bSugar = bSugar;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	
}
