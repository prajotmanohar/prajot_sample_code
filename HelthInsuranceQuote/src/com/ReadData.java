package com;

public class ReadData {
	private String name;
	private String gender;
	private String age;
	private Habits habit;
	private HealthCondition health;
	
	public ReadData(String name, String gender, String age, Habits habit, 
			HealthCondition health) {
		this.name=name;
		this.gender=gender;
		this.age=age;
		this.habit=habit;
		this.health=health;
		
	}
	
	public Habits getHabit() {
		return habit;
	}
	public void setHabit(Habits habit) {
		this.habit = habit;
	}
	public HealthCondition getHealth() {
		return health;
	}
	public void setHealth(HealthCondition health) {
		this.health = health;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
}
